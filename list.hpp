///////////////////////////////////////////////////////////////////////////////
/////////// University of Hawaii, College of Engineering
/////////// EE 205  - Object Oriented Programming
/////////// Lab 08 - Cat Wrangler
///////////
/////////// @file list.hpp
/////////// @version 1.0
///////////
/////////// Header file for the list class
///////////
/////////// @author Alexander Sidelev <asidelev@hawaii.edu>
/////////// @brief  Lab 08 - Cat Wrangler - EE 205 - Spr 2021
/////////// @date   05_19_2021
///////////////////////////////////////////////////////////////////////////////////////
//////
////
#pragma once
#include "node.hpp"
#include <cstdlib> 

namespace animalfarm {



class DoubleLinkedList {
   public:
//      DoubleLinkedList();
//      ~DoubleLinkedList();
      const bool empty() const;
      void push_front(Node* newNode);
      Node* pop_front();
      Node* get_first() const;
      Node* get_next( const Node* currentNode ) const;
      inline unsigned int size() const {
         unsigned int count3 = 0;
         Node* current = head;
         while(current != nullptr) {
            current = current->next;
         }//while loop
         count3++;
         return count3;
         };
      void push_back( Node* newNode);
      Node* pop_back();
      Node* get_last() const;
      Node* get_prev( const Node* currentNode ) const;
      const bool validate() const;
      void dump() const;
      const bool isIn(Node* aNode ) const;
      void insert_after(Node* currentNode, Node* newNode );
      void insert_before(Node* currentNode, Node* newNode );
      void swap(Node* node1, Node* node2);
   protected:
      unsigned int count;
      Node* head = nullptr;
      Node* tail = nullptr;
}; // DoubleLinkedList
}//animalfarm namespace
 
