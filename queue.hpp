///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 08a - Cat Wrangler
///
/// @file queue.hpp
/// @version 1.0
///
/// A generic Queue collection class.
///
/// @author Alexander Sidelev <asidelev@hawaii.edu>
/// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/// @date   05_19_2021
///////////////////////////////////////////////////////////////////////////////

#include "node.hpp"
#include "list.hpp"

#pragma once
using namespace animalfarm;
class Queue {
protected:
	DoubleLinkedList list = DoubleLinkedList();
public:
	inline const bool empty() const { return list.empty(); };
	// Using the above as an example, bring over the following methods from list:
	// Add size()
   inline unsigned int size() const {return list.size();};
	// Add push_front()
   inline void push_front(Node* newNode) {return;}; 
	// Add pop_back()
   inline Node* pop_back(){ return list.pop_back();};
	// Add get_first()
   inline Node* get_first() const {return list.get_first();};
	// Add get_next()
   inline Node* get_next( const Node* currentNode ) const { return list.get_first();};




}; // class Queue
