///////////////////////////////////////////////////////////////////////////////
/////////// University of Hawaii, College of Engineering
/////////// EE 205  - Object Oriented Programming
/////////// Lab 07a - Animal Farm 2
///////////
/////////// @file list.cpp
/////////// @version 1.0
///////////
/////////// Body file for the List class
///////////
/////////// @author Alexander Sidelev <asidelev@hawaii.edu>
/////////// @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
/////////// @date   05_19_2021
///////////////////////////////////////////////////////////////////////////////////////
#include <iostream>
#include <cstddef>
#include <cassert>
#include "list.hpp"
using namespace std;
namespace animalfarm {
//   unsigned int DoubleLinkedList::count;  


   void DoubleLinkedList::swap(Node* node1, Node* node2) {
      if( node1 == nullptr || node2 == nullptr) {
         return;
      }//trivial case empty list

      assert(isIn(node1) && isIn(node2));

      if(node1 == node2)
         return; //trivial one node list
      
      if(node1 == head && node2 == tail && count == 2){
         head = node2;
         tail = node1;
         node1->next = nullptr;
         node2->prev = nullptr;
         node2->next = node1;
         node1->prev = node2;
         return;
      
      }//special case only 2 node list

 
 // I can't figure this one out
      if(node1 == head && node2 == tail && count > 2){
         cout << "Special Case" << endl;
         head = node2;
         tail = node1;
         node2->prev->next = node1;
         node1->next->prev = node2;
         node2->next = node1->next; // or head->next
         node1->prev = node2->prev; // or tail->next
         node2->prev = nullptr; // goes to null since its now head
         node1->next = nullptr;// goes to null since its now tail
         
         
         return;

      }//special case head & tail mode than 2 node in list

      if(node1 == head && node2 != tail && node2->prev == node1 && count > 2) {
         head = node2;
         node1->next = node2->next;
         node2->next = node1;
         node2->prev = nullptr;
         node1->prev = node2;
         node1->next->prev = node1;
         return;
      }// node1 is head and node2 is next to it

      if(node1 == head && node2 != tail && node2->prev != node1 && count > 2) {
         head = node2;
         node1->next = node2->next;
         node2->next = node1->next;
         node1->prev = node2->prev;
         node2->prev = nullptr;
         node2->next->prev = node2;
         node1->next->prev = node1;
         node1->prev->next = node1;
         return;
      
      }//node1 head and node2 not next to it

      if(node1 != head && tail == node2 && count > 2){
      //   node1 = node2;
         tail = node1;
         node2->next = node1->next;
         node2->prev = node1->prev;
      //   tail = node1;
         node1->next = nullptr;
         node1->prev = node2->prev;
      //   return;   
      }

      //special case node1 and node2 are next to each other
   
   //   if(node1->next == node2 && node2->prev == node1 && count > 2){
      if(node1->prev == node1 && node2->next == node2 && count > 2){
         node1->prev = node2;
         node2->next = node1;
         return;
      }
      
      if(node1->next == node1 && node2->prev == node2 && count > 2){
         node1->next = node2;
         node2->prev = node1;
         return;
      };

      { //general case
         cout << "General Case" << endl;
         node1 = node2;
         node2->next = node1->next;
         node2->prev = node1->prev;

         node2 = node1;
         node1->next = node2->next;
         node1->prev = node2->prev;
         return;
      }//gen case

   }//swap() fxn











   const bool DoubleLinkedList::empty() const {
    
      return head == nullptr; 
      return tail == nullptr;
   } //empty() fxn
  
  
   void DoubleLinkedList::push_front(Node* newNode) {
      
      if(newNode == nullptr ){
         return;
      }
      if(head != nullptr) {
         newNode->next = head;
         newNode->prev = nullptr;
         head->prev = newNode;
         head = newNode;
      }else{
         newNode->next = nullptr;
         newNode->prev = nullptr;
         head = newNode;
         tail = newNode;
      }
      count++;
      validate();
   } // push_front()
   
   Node* DoubleLinkedList::pop_front() {
      if ( head == nullptr){
         return nullptr;
      }//base case

      Node* meow = head;

      if (head == tail){
         head = nullptr;
         tail = nullptr;
         meow->prev = nullptr;
         meow->next = nullptr;
      }else{
   
         head = head->next;
         head->prev = nullptr;
         meow->next = nullptr;
         meow->prev = nullptr;
      }
      count--;
      return meow;
   
   }//pop_front()

   Node* DoubleLinkedList::get_first() const { 
      return head;
   }//get_first()
 
  // Node* currentNode = new Node();

   Node* DoubleLinkedList::get_next(const Node* currentNode ) const {
      
      return currentNode->next;
   }//get_next() fxn

   Node* DoubleLinkedList::get_prev(const Node* currentNode ) const {
      return currentNode->prev;
   }//get_prev fxn

   
/*   unsigned int DoubleLinkedList::size() const {
      
      Node* current = head;
      while(current != nullptr) {
         count++;
         current = current->next;
      }//while loop
      return count;
   }
*/
   void DoubleLinkedList::push_back( Node* newNode){
      
      if( newNode == nullptr )
         return;
      if( tail != nullptr) {
         newNode->next = nullptr;
         newNode->prev = tail;
         tail->next = newNode;
         tail = newNode;
      }else{
         newNode->next = nullptr;
         newNode->prev = nullptr;
         head = newNode;
         tail = newNode;
      }//else
      count++;
      validate();
   }//push_back fxn

   const bool DoubleLinkedList::validate() const {
      if( head == nullptr ) {
         assert( tail == nullptr );
         assert( count == 0 );
      }else{
         assert (tail != nullptr );
         assert(count != 0 );
      }

      if( tail == nullptr ) {
         assert( head == nullptr );
         assert( count == 0 );
      }else{
         assert( head != nullptr );
         assert( count != 0 );
      }

      if( head != nullptr && tail == head) {
         assert( count == 1);
      }
      unsigned int forwardCount = 0;
      Node* currentNode = head;
      while( currentNode != nullptr ) {
         forwardCount++;
         if(currentNode->next != nullptr ) {
            assert( currentNode->next->prev == currentNode );
         }
         currentNode = currentNode->next;
      }
      cout << "FC = " << forwardCount << endl;
      cout << "Count = " << count << endl;
      assert( count == forwardCount );

      unsigned int backwardCount = 0;
      currentNode = tail;
      while( currentNode != nullptr ) {
         backwardCount++;
         if( currentNode->prev != nullptr) {
            assert( currentNode->prev->next == currentNode );
         }
         currentNode = currentNode->prev;
      }
      cout << "BC = " << forwardCount << endl;
     // cout << "Count = " << count << endl;
      assert( count == backwardCount );
    //  cout << "Function Validated" << endl;
      return true;
   

   }//validate fxn

   void DoubleLinkedList::dump() const {
      cout << "DoubleLinkedList: head=[" << head << "]   tail=[" << tail << "]" << endl;
      for( Node* currentNode = head ; currentNode != nullptr; 
            currentNode = currentNode->next){
         cout << " ";
         currentNode->dump();
      }//for-loop
   }//dump fxn

   const bool DoubleLinkedList::isIn(Node* aNode) const {
      Node* currentNode = head;
      while( currentNode != nullptr ) {
         if( aNode == currentNode )
            return true;
         currentNode = currentNode->next;
      }
      return false;
   }//isIn fxn

   void DoubleLinkedList::insert_after(Node* currentNode, Node* newNode ) {
      if( currentNode == nullptr && head == nullptr ) {
         push_front( newNode );
         return;
      }
      if( currentNode != nullptr && head == nullptr) {
         assert( false );
      }
      if( currentNode == nullptr && head != nullptr) {
         assert(false);
      }
         assert( currentNode != nullptr && head != nullptr );
         assert( isIn( currentNode ));
         assert( !isIn( newNode ));

      if( tail != currentNode ) {
         newNode->next = currentNode->next;
         currentNode->next = newNode;
         newNode->prev = currentNode;
         newNode->next->prev = newNode;
      }else{
         push_back( newNode );
      }
      validate();
   }//insert_after

   void DoubleLinkedList::insert_before(Node* currentNode, Node* newNode ) {
      if( currentNode == nullptr && tail == nullptr ) {
         push_back( newNode );
         return;
      }//if1

      if( currentNode != nullptr && tail == nullptr) {
         assert( false);
      }//if2

      if(currentNode == nullptr && tail != nullptr ){
         assert(false);
      }

      assert( currentNode != nullptr && head != nullptr );

      assert( isIn( currentNode ));
      assert(!isIn( newNode ));
      
      if( head != currentNode) {
         newNode->prev = currentNode->prev;
         currentNode->prev = newNode;
         newNode->next->prev = newNode;
      }else{
         push_front( newNode );
      }
      validate();

   }//insert_before

   Node* DoubleLinkedList::pop_back() {
      if( tail == nullptr) {
         return nullptr;
      }

      Node* meow = tail;

      if (tail == head){
         tail = nullptr;
         head = nullptr;
         meow->next = nullptr;
         meow->prev = nullptr;
      }else{

         tail = tail->prev;
         tail->next = nullptr;
         meow->prev = nullptr;
         meow->next = nullptr;
      }
      count--;
      return meow;
   }//pop_back fxn

   Node* DoubleLinkedList::get_last() const {
     return tail;
   }//get_last fxn


} // animalfarm

